Arsitektur style: RESTful
todo {
    description: ...
    created_at: ...
    started_at: ...
    finished_at: ...
}

Create Todo - buat todo baru
    POST /todo
    request body:
        { description: ... }
    return: 201 Created if success
    {todo}

Update Todo - update todo yan gsudah adas
    PATCH /todo/:id
    request body:
        { description: ..., 
          status: "start"
                  "finish"
                  "undo" }
    return: 200 if success
    {todo}
 
List/Search/Filter Todo
    GET /todo?status=[created,started,finished]&keyword=...
    return: 200 if success
    response body:
    [{todo}]

Read Todo
    GET /todo/:id
    return: 200 if success
    response body:
    {todo}

Delete Todo
    DELETE /todo/:id
    return: 204 if success
